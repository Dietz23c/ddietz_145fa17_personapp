

import java.util.InputMismatchException;
import java.util.Scanner;


// include any necessary import statements

/**
 * Application (driver) class used to test the capabilities of class Person
 *
 * @author 
 * @version 
 */
public class PersonTest {
    
	public static void main( String[] args )
	{
		// instantiate Scanner object for reading in keyboard input
                Scanner input = new Scanner( System.in );
           
                int ssn1 = PersonTest.readSSN(input);
                boolean voterStatus1 = PersonTest.readVoterStatus(input);
                double readsalary1 = PersonTest.readAnnualSalary(input);
                // read in the name 
                
                //if we are callin ga static method from within the calss in 
                //which taht static method has been defined we don't need to 
                //use ClassNmae. methodName() as the method call (we don't NEED 
                //to use dot notation). But!!! It won't hurt to do so:
                
                String name1 = readName( input );// local reference-type variables are null by default
                
                
		// prompt user to input SSN
                System.out.print( "Enter the first person's 9-digit SSN (no spaces or dashes): " );
                
		
                
		// prompt user to input annual salary
                System.out.print( "Enter the first person's annual salary as a decimal value: " );
                
		
                
		// Create new Person object using the parameters above, and assign 
		// object reference to newly declared person1 object variable
                Person person1 = new Person( name1, ssn1, voterStatus1, salary1 );

		// Create new Person object using the parameters given in the
		// homework instructions, and assign object reference to person2
                Person person2 = new Person( "Barry Obama", 987654321, false, -123.45);
                

		// Print a blank line
                System.out.println();
                
		// Display person1's information
                Person.displayInfo( person1 );

		// Print a blank line
                System.out.println();
                

		// Display person2's information
                Person.displayInfo( person2 );

		// update person2's information by calling each of the
		// four instance methods
                person2.setName( "Barack Hussein Obama" );
                person2.setSocialSecurityNumber( 123456789 );
                person2.setVoterRegistrationStatus( true );
                person2.setAnnualSalary( 400000.00 );
                
		// Print a blank line
		System.out.println();
                
		// Print a message indicating that person2's info has been changed
                System.out.println("Person 2's information has been changed to: ");
                
		// Print a blank line
                System.out.println();
				
		// Display person2's updated information 
                Person.displayInfo(person2);

	} // end method main
        
        /*
        This method ahs to be defined a static becasue we are not 
        instantiating any PersonTest objects! (Thus, the readName method 
        belongs to the PersonTest calss as a whole.)
        */
private static String readName( Scanner input )
{
    // prompt user to input name
                System.out.print( "Enter the first person's name: " );
                
    do {
                    if ( input.hasNextLine () ) {
                        name = input.nextLine(); // read in the line of text
                        break; // exits the otherwise infinite do-while loop
                    } else {
                        System.out.println( "Error! Invalid name. Try again: ");
                        input.nextLine(); // discards entire line (don't assign it to anything)
                        continue; // unnecessary! 
                    } // end if/else
                    
                    // there are no statements to skip... so....
                    //System.out.println("I am skipped!");
                         
                } while (true); 
            
            return name;
}// end method readName
    private static int readSSN( Scanner input )
    {
       // read in the SSN
                int ssn1 = 0; // initialize because local primitive-type variables have no default value 
                boolean isValid = false; // more elegent than using break
                while ( !isValid ) 
                {// while isValid is NOT true...
                    if ( input.hasNextInt() ) { // did the user type in an int?
                        ssn1 = input.nextInt(); // read in the int
                        input.nextLine(); // discard other entered data, if any
                        isValid = true; // this allows us to exit the loop 
                    } else {
                        System.out.println ("Error! Invalid SSN. Try agian ");
                        input.nextLine(); // discard the entire line (assuming the user didn't type in an int)
                    
                    }
                }
                 // end while 
    return ssn;
    
} // end class PersonTest



    private static boolean readVoterStatus( Scanner input )
{
  boolean voterStatus1 = false; // local primitive-type variables have no defualt value,
                                              // so we neeed to assign one as an initial value for voterStatus1
                do {
                    if ( input.hasNextBoolean () ) {
                        voterStatus1 = input.nextBoolean(); // read in the line of text
                        input.nextLine(); // discard ther entered data, if any
                        break; // exits the otherwise infinite do-while loop
                    } else {
                        System.out.println( "Error! Invalid registration status. Try again: ");
                        input.nextLine(); // discards entire line (don't assign it to anything)
                        //continue; // unnecessary! 
                    } // end if/else
                }while (true);
                
        return voterStatus;
 
}
    private static double readAnnualSalary( Scanner input )
            {
                // read in the salary amount
                double salary1 = 0.0;
                
                // use exception handling for reading in a valid annual salary
                boolean isValidSalary = false; // will update to true when a valid salary has been input 
                
                while ( !isValidSalary ) { // loop will continue to repeat until isValidSalary == ture
                    // prompt user to input annual salary
                    System.out.print( "Enter the first person's annual salary as a decimal value: " );
                    
                    try {
                        // inside a try block, you'll code the statements 
                        // that could potentially throw an exception 
                        salary1 = input.nextDouble();
                        isValidSalary = true;
                    } catch( InputMismatchException e ) {
                        System.out.println(" Error! Please try agian. ");   
                    }// end try 
                    
                    input.nextLine(); // discard rest of line, regardless of whether 
                                      // an exception was thrown
                    
                }// end while
            }
    
}